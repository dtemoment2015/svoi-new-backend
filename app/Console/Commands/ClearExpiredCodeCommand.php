<?php

namespace App\Console\Commands;

use App\Models\Code;
use Illuminate\Console\Command;

class ClearExpiredCodeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:clear-expired-code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Очистка просроченных кодов';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Code::expired()->delete();
        return Command::SUCCESS;
    }
}
