<?php

namespace App\Console\Commands;

use App\Models\Code;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class deleteCodeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete-code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete-code';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table('users')->delete();
        DB::table('codes')->delete();

        return Command::SUCCESS;
    }
}
