<?php

namespace App\Providers;

use App\Models\Action\Comment;
use App\Models\Content\Article;
use App\Models\Content\Category;
use App\Observers\v1\ArticleObserver;
use App\Observers\v1\CategoryObserver;
use App\Observers\v1\CommentObserver;
use App\Services\PlatformDeviceService;
use App\Services\SmsService;
use App\Services\TelegramLogService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Sms', SmsService::class);
        $this->app->bind('TelegramLog', TelegramLogService::class);
        $this->app->bind('PlatformDevice', PlatformDeviceService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Article::observe(ArticleObserver::class);
        Category::observe(CategoryObserver::class);
        Comment::observe(CommentObserver::class);
    }
}
