<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class PlatformDevice extends Facade
{
   protected static function  getFacadeAccessor()
   {
      return 'PlatformDevice';
   }
}
