<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class TelegramLog extends Facade
{
   protected static function  getFacadeAccessor()
   {
      return 'TelegramLog';
   }
}
