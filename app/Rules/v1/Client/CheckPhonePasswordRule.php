<?php

namespace App\Rules\v1\Client;

use Auth;
use Illuminate\Contracts\Validation\InvokableRule;
use Log;

class CheckPhonePasswordRule implements InvokableRule
{
    public $phone;

    public function __construct($phone = null)
    {
        $this->phone = $phone;
    }
    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        if (!Auth::attempt(['phone' => $this->phone, 'password' => $value])) {
            return $fail(__('Логин и пароль неправельный'));
        }
    }
}
