<?php

namespace App\Rules\v1;

use App\Models\Code;
use Illuminate\Contracts\Validation\Rule;

class CheckCodeRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */


    protected $code;

    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->code = Code::wherePhone($value)
            ->active()
            ->limit(1)
            ->first();

        return (bool) !$this->code;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.code_check', ['second' => $this->code->expired_at->unix() -  now()->unix()]);
    }
}
