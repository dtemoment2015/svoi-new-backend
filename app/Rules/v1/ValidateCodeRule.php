<?php

namespace App\Rules\v1;

use App\Models\Code;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class ValidateCodeRule implements Rule
{

    public $request;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($code = Code::wherePhone($this->request->input('phone'))
            ->active()
            ->limit(1)
            ->first()
        ) {
            $this->request->request->add(['_code' => $code]);
        }

        return Hash::check($value, optional($code)->code);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Неверный код';
    }
}
