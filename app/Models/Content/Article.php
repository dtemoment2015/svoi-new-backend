<?php

namespace App\Models\Content;

use App\Models\Action\Comment;
use App\Models\Action\Rate;
use App\Models\System\Status;
use App\Models\User;
use App\Traits\ColumnFillable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * App\Models\Content\Article
 * @OA\Schema (schema="_ModelArticle")
 * @property int $id
 * @property string $publication_at
 * @property int $active
 * @property int $moderated
 * @property int $status_id
 * @property int $category_id
 * @property string $name
 * @property string $slug
 * @property string|null $annotation
 * @property string|null $body
 * @property int $user_id
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereAnnotation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereModerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article wherePublicationAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\Content\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Rate[] $rates
 * @property-read int|null $rates_count
 * @property-read Status $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Content\Tag[] $tags
 * @property-read int|null $tags_count
 * @property-read User $user
 * @method static \Illuminate\Database\Query\Builder|Article onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|Article withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Article withoutTrashed()
 * @property int|null $rubric_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Content\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read mixed $preview
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|Media[] $media
 * @property-read int|null $media_count
 * @property-read \App\Models\Content\Rubric|null $rubric
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereRubricId($value)
 */
class Article extends Model implements HasMedia
{
    use HasFactory, ColumnFillable, SoftDeletes, InteractsWithMedia;

    protected $casts = [
        'active' => 'bool',
        'moderated' => 'bool',
        'publication_at' => 'datetime',
    ];

    protected $appends = ['preview'];

    /**
     * 
     *  @OA\Property(
     *    property="id",
     *    type="int",
     *    example="0",
     *    description="ID Пользователя"
     *  ) 
     *  
     *  
     *  @OA\Property(
     *    property="publication_at",
     *    type="datetime",
     *    example="2022-11-08T10:16:09.000000Z",
     *    description="Дата публикации"
     *  ) 
     *  
     *  @OA\Property(
     *    property="preview",
     *    type="string",
     *    example="https://localhost/image.jpg",
     *    description="Постер статьи"
     *  ) 
     *  
     *  @OA\Property(
     *    property="active",
     *    type="boolean",
     *    example="true",
     *    description="Активность новости"
     *  ) 
     *  
     *  @OA\Property(
     *    property="moderated",
     *    type="boolean",
     *    example="true",
     *    description="Прошел модерацию"
     *  ) 
     *  
     *  @OA\Property(
     *    property="name",
     *    type="string",
     *    example="Первая новость",
     *    description="Название"
     *  ) 
     *  
     *  @OA\Property(
     *    property="annotation",
     *    type="string",
     *    example="Пришла весна, пора гулять",
     *    description="Краткое описание"
     *  ) 
     *  
     *  @OA\Property(
     *    property="body",
     *    type="string",
     *    example="<p>Контент данной новости</p>",
     *    description="HTML-контент"
     *  ) 
     *  
     *  @OA\Property(
     *    property="position",
     *    type="int",
     *    example="0",
     *    description="Позиция"
     *  ) 
     *  
     *  @OA\Property(
     *    property="slug",
     *    type="string",
     *    example="pervaya-novost",
     *    description="Ссылка"
     *  ) 
     *  
     *  @OA\Property(
     *    property="status",
     *    type="object",
     *    example="null",
     *    description="Статус"
     *  ) 
     *  
     *  @OA\Property(
     *    property="rubric",
     *    type="object",
     *    ref="#/components/schemas/_ModelRubric"
     *  ) 
     *  
     *  @OA\Property(
     *    property="tags",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/_ModelTag"),
     *    description="Теги"
     *  ) 
     *  
     *  @OA\Property(
     *    property="categories",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/_ModelCategory"),
     *    description="Связанные катгории"
     *  ) 
     *  
     *  
     */


    public function rubric()
    {
        return $this->belongsTo(Rubric::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function rates()
    {
        return $this->morphMany(Rate::class, 'rateable');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function registerMediaConversions(
        Media $media = null
    ): void {

        $this->addMediaConversion('medium')
            ->width(400)
            ->nonQueued();

        $this->addMediaConversion('small')
            ->width(200)
            ->nonQueued();
    }

    public function registerMediaCollections(Media $media = null): void
    {

        $this->addMediaCollection('preview')
            ->singleFile();
    }

    public function getPreviewAttribute()
    {
        return optional(optional($this->getMedia('preview'))->last())->original_url;
    }

    public function setPreviewUrlAttribute($val)
    {
        try {
            $this
                ->addMediaFromUrl($val)
                ->toMediaCollection('preview');
        } catch (\Throwable $th) {
        }
    }
}
