<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::enforceMorphMap([
    'category' => Category::class,
    'article' => Article::class
]);
/**
 * App\Models\Content\Tag
 * @OA\Schema (schema="_ModelTag")
 * @property int $id
 * @property int $active
 * @property int $user_id
 * @property string $name
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Tag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tag query()
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Content\Article[] $articles
 * @property-read int|null $articles_count
 * @property string|null $slug
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereSlug($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Content\Category[] $categories
 * @property-read int|null $categories_count
 */
class Tag extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * 
     *  @OA\Property(
     *    property="id",
     *    type="int",
     *    example="0",
     *    description="ID"
     *  ) 
     *  
     *  @OA\Property(
     *    property="name",
     *    type="string",
     *    example="Новинки",
     *    description="Название"
     *  ) 
     *  
     *  @OA\Property(
     *    property="slug",
     *    type="string",
     *    example="novinki",
     *    description="Название"
     *  ) 
     *  
     */


    public function articles()
    {
        return $this->morphedByMany(Article::class, 'taggable');
    }

    public function categories()
    {
        return $this->morphedByMany(Category::class, 'taggable');
    }
}
