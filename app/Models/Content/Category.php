<?php

namespace App\Models\Content;

use App\Models\User;
use App\Traits\ColumnFillable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Content\Category
 *
 * @OA\Schema (schema="_ModelCategory")
 * @property int $id
 * @property int $active
 * @property int $user_id
 * @property int $category_id
 * @property string $name
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $categories
 * @property-read int|null $categories_count
 * @property-read Category $category
 * @method static \Illuminate\Database\Query\Builder|Category onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Category withoutTrashed()
 * @property string|null $slug
 * @property int $position
 * @method static \Illuminate\Database\Eloquent\Builder|Category wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereSlug($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Content\Article[] $articles
 * @property-read int|null $articles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Content\Tag[] $tags
 * @property-read int|null $tags_count
 */
class Category extends Model
{
    use HasFactory, SoftDeletes, ColumnFillable;

    public $timestamps = false;

    /**
     * 
     *  @OA\Property(
     *    property="id",
     *    type="int",
     *    example="0",
     *    description="ID"
     *  ) 
     *  
     *  @OA\Property(
     *    property="active",
     *    type="boolean",
     *    example="true",
     *    description="Активность новости"
     *  ) 
     *  
     *  @OA\Property(
     *    property="name",
     *    type="string",
     *    example="Первая новость",
     *    description="Название"
     *  ) 
     *  
     *  @OA\Property(
     *    property="position",
     *    type="int",
     *    example="0",
     *    description="Позиция"
     *  ) 
     *  
     *  @OA\Property(
     *    property="slug",
     *    type="string",
     *    example="pervaya-novost",
     *    description="Ссылка"
     *  ) 
     *  
     *  @OA\Property(
     *    property="tags",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/_ModelTag"),
     *    description="Теги"
     *  ) 
     *  
     *  @OA\Property(
     *    property="category",
     *    type="object",
     *    ref="#/components/schemas/_ModelCategory",
     *    description="Родительная категория категории"
     *  ) 
     *  
     *  @OA\Property(
     *    property="categories",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/_ModelCategory"),
     *    description="Дочерние категории"
     *  ) 
     * 
     */


    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
