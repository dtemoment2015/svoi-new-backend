<?php

namespace App\Models\Content;

use App\Traits\ColumnFillable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Content\Rubric
 * @OA\Schema (schema="_ModelRubric")
 * @property int $id
 * @property int $active
 * @property string $name
 * @property string|null $slug
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Content\Article[] $articles
 * @property-read int|null $articles_count
 * @method static \Illuminate\Database\Eloquent\Builder|Rubric newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Rubric newQuery()
 * @method static \Illuminate\Database\Query\Builder|Rubric onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Rubric query()
 * @method static \Illuminate\Database\Eloquent\Builder|Rubric whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Rubric whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Rubric whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Rubric whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Rubric wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Rubric whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|Rubric withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Rubric withoutTrashed()
 * @mixin \Eloquent
 */
class Rubric extends Model
{
    use HasFactory, ColumnFillable, SoftDeletes;

    public $timestamps = false;
    
    /**
     * 
     *  @OA\Property(
     *    property="id",
     *    type="int",
     *    example="0",
     *    description="ID"
     *  ) 
     *  
     *  @OA\Property(
     *    property="name",
     *    type="string",
     *    example="Новинки",
     *    description="Название"
     *  ) 
     *  
     *  @OA\Property(
     *    property="slug",
     *    type="string",
     *    example="novinki",
     *    description="Название"
     *  ) 
     *  
     */

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
