<?php

namespace App\Models\Action;

use App\Models\Content\Article;
use App\Models\User;
use App\Traits\ColumnFillable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletes;

Relation::enforceMorphMap([
    'article' => Article::class,
    'user' => User::class,
    'comment' => sef::class
]);

/**
 * App\Models\Action\Comment
 * @OA\Schema (schema="_ModelComment")
 * @property int $id
 * @property int $active
 * @property int $rate
 * @property int $moderated
 * @property int $comment_id
 * @property int $status_id
 * @property int $user_id
 * @property string $model_type
 * @property int $model_id
 * @property string|null $name
 * @property string|null $body
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCommentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereModelType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereModerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereUserId($value)
 * @mixin \Eloquent
 * @property string $commentable_type
 * @property int $commentable_id
 * @property-read Comment $comment
 * @property-read Model|\Eloquent $commentable
 * @property-read \Illuminate\Database\Eloquent\Collection|Comment[] $comments
 * @property-read int|null $comments_count
 * @method static \Illuminate\Database\Query\Builder|Comment onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCommentableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCommentableType($value)
 * @method static \Illuminate\Database\Query\Builder|Comment withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Comment withoutTrashed()
 * @property-read User $user
 */
class Comment extends Model
{
    use HasFactory, ColumnFillable, SoftDeletes;

    /**
     * 
     *  @OA\Property(
     *    property="id",
     *    type="int",
     *    example="0",
     *    description="ID"
     *  ) 
     *  
     *  @OA\Property(
     *    property="body",
     *    type="string",
     *    example="Супер! Мне очень нравится",
     *    description="Комментарий"
     *  ) 
     *  
     *  @OA\Property(
     *    property="user",
     *    type="object",
     *    ref="#/components/schemas/_ModelUser",
     *    description="Кто оставил комментарий"
     *  ) 
     *  
     * 
     */

    public function commentable()
    {
        return $this->morphTo();
    }

    public function comments()
    {
        return $this->hasMany(self::class);
    }

    public function comment()
    {
        return $this->belongsTo(self::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
