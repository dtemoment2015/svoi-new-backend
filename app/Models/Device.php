<?php

namespace App\Models;

use App\Traits\ColumnFillable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Device
 * @OA\Schema (schema="_ModelDevice")
 * @property int $id
 * @property string $device_id
 * @property int $platform_id
 * @property string $locale
 * @property string $country
 * @property int $timezone
 * @property string $push_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Platform $platform
 * @method static \Illuminate\Database\Eloquent\Builder|Device newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Device newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Device query()
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device wherePlatformId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device wherePushToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 */
class Device extends Model
{
    use HasFactory, ColumnFillable;
    /**
     *  
     *  @OA\Property(
     *    property="device_id",
     *    type="string",
     *    example="5FE23DE34LVL",
     *    description="ID Устройства"
     *  ) 
     *  
     *  @OA\Property(
     *    property="locale",
     *    type="string",
     *    example="ru",
     *    description="Локализация устройства"
     *  ) 
     *  
     *  @OA\Property(
     *    property="country",
     *    type="string",
     *    example="ru",
     *    description="Код странцы"
     *  ) 
     *  
     *  @OA\Property(
     *    property="timezone",
     *    type="integer",
     *    example="18000",
     *    description="Временная зона. Чтобы получить +5, +3 и тд. Нужно поделить на 3600"
     *  ) 
     *  
     *  @OA\Property(
     *    property="push_token",
     *    type="string",
     *    example="",
     *    description="Ключ для Push - уведомлений"
     *  ) 
     *  
     *  @OA\Property(
     *    property="platform",
     *    type="object",
     *    ref="#/components/schemas/_ModelPlatform",
     *    description="Платформа"
     *  ) 
     *  
     */

    public function platform()
    {
        return $this->belongsTo(
            Platform::class
        );
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
