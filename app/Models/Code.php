<?php

namespace App\Models;

use App\Traits\ColumnFillable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Code
 * @OA\Schema (schema="_ModelCode")
 * @method static \Illuminate\Database\Eloquent\Builder|Code newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Code newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Code query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $phone
 * @property int $code
 * @property string|null $exchange_id
 * @property string $expired_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Code whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Code whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Code whereExchangeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Code whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Code whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Code wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Code whereUpdatedAt($value)
 * @property string $first_name
 * @property string $last_name
 * @method static \Illuminate\Database\Eloquent\Builder|Code active()
 * @method static \Illuminate\Database\Eloquent\Builder|Code expired()
 * @method static \Illuminate\Database\Eloquent\Builder|Code whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Code whereLastName($value)
 */
class Code extends Model
{
    const EXPIRED_AT = 60*24; //minutes

    use HasFactory, ColumnFillable;

    protected $casts = ['expired_at' => 'datetime'];

    protected $hidden = ['code'];

    /**
     *  
     *  @OA\Property(
     *    property="phone",
     *    type="string",
     *    example="998903467899",
     *    description="Номер телефона пользователя"
     *  ) 
     *  @var string
     *  
     *  @OA\Property(
     *    property="first_name",
     *    type="string",
     *    example="",
     *    description="Имя пользователя"
     *  ) 
     *  @var string
     *  
     *  @OA\Property(
     *    property="last_name",
     *    type="string",
     *    example="",
     *    description="Фамилия пользователя"
     *  ) 
     *  @var string
     *  
     *  @OA\Property(
     *    property="expired_at",
     *    type="datetime",
     *    example="2022-11-08T10:16:09.000000Z",
     *    description="Дата окончания токена"
     *  ) 
     *  @var string
     */

    public function setCodeAttribute($val)
    {
        if ($val) {
            $this->attributes['code'] = bcrypt($val);
        }
    }

    public function scopeActive($query)
    {
        return $query->whereRaw('expired_at >= CURRENT_TIMESTAMP()');
    }

    public function scopeExpired($query)
    {
        return $query->whereRaw('expired_at < CURRENT_TIMESTAMP()');
    }
}
