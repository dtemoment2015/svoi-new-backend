<?php

namespace App\Models;

use App\Models\Action\Comment;
use App\Models\Action\Rate;
use App\Models\Content\Article;
use App\Traits\ColumnFillable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

//   Модели в схемы лучше не выводить - только ресусры. Тут можно целиком описать


/**
 * App\Models\User
 *
 * @OA\Schema (schema="_ModelUser")
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $middle_name
 * @property bool $active
 * @property string $phone
 * @property \Illuminate\Support\Carbon|null $birthday
 * @property string|null $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|Article[] $acrticles
 * @property-read int|null $acrticles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Device[] $devices
 * @property-read int|null $devices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Rate[] $rates
 * @property-read int|null $rates_count
 */


class User extends Authenticatable
{

    use HasApiTokens, HasFactory, Notifiable, ColumnFillable, SoftDeletes;

    /**
     * 
     *  @OA\Property(
     *    property="id",
     *    type="int",
     *    example="0",
     *    description="ID Пользователя"
     *  ) 
     *  @var int
     *  
     *  
     *  @OA\Property(
     *    property="phone",
     *    type="string",
     *    example="998903467899",
     *    description="Номер телефона пользователя"
     *  ) 
     *  @var string
     *  
     *  
     *  @OA\Property(
     *    property="email",
     *    type="string",
     *    example="test@test.ru",
     *    description="Email пользователя"
     *  ) 
     *  @var string
     *  
     *  @OA\Property(
     *    property="first_name",
     *    type="string",
     *    example="",
     *    description="Имя пользователя"
     *  ) 
     *  @var string
     *  
     *  @OA\Property(
     *    property="last_name",
     *    type="string",
     *    example="",
     *    description="Фамилия пользователя"
     *  ) 
     *  @var string
     *  
     *  @OA\Property(
     *    property="active",
     *    type="boolean",
     *    example="false",
     *    description="Активность пользователя"
     *  ) 
     *  @var string
     *  
     *  @OA\Property(
     *    property="middle_name",
     *    type="string",
     *    example="",
     *    description="Отчество пользователя"
     *  ) 
     *  @var string 
     */

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'active' => 'bool',
        'birthday' => 'date',
    ];

    public function devices()
    {
        return $this->belongsToMany(Device::class);
    }

    public function acrticles()
    {
        return $this->hasMany(Article::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function rates()
    {
        return $this->hasMany(Rate::class);
    }

    public function setPasswordAttribute($val)
    {
        if ($val) {
            $this->attributes['password'] = bcrypt($val);
        }
    }

    public function setPhoneAttribute($val)
    {
        if ($val) {
            $this->attributes['phone'] = str_replace('+', '', $val);
        }
    }
}
