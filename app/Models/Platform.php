<?php

namespace App\Models;

use App\Traits\ColumnFillable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Platform
 *
 * @OA\Schema (schema="_ModelPlatform")
 * @property int $id
 * @property string $code
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Platform newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Platform newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Platform query()
 * @method static \Illuminate\Database\Eloquent\Builder|Platform whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Platform whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Platform whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Platform whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Platform whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Platform whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Device[] $devices
 * @property-read int|null $devices_count
 * @method static \Illuminate\Database\Query\Builder|Platform onlyTrashed()
 * @method static \Illuminate\Database\Query\Builder|Platform withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Platform withoutTrashed()
 */
class Platform extends Model
{
    use HasFactory, ColumnFillable, SoftDeletes;

    /**
     * 
     *  @OA\Property(
     *    property="code",
     *    type="string",
     *    example="ios",
     *    description="Код платформы"
     *  ) 
     *  
     *  @OA\Property(
     *    property="name",
     *    type="string",
     *    example="iOS устройство",
     *    description="Название платформы"
     *  ) 
     *  
     */

    public function devices()
    {
        return $this->hasMany(Device::class);
    }
}
