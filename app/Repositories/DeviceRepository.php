<?php

namespace App\Repositories;

use App\Models\Device;
use App\Models\Platform;
use App\Repositories\Interfaces\DeviceRepositoryInterface;


class DeviceRepository implements DeviceRepositoryInterface
{

   public function storeDevice($request)
   {
      $Platform = Platform::whereCode($request->platform_code)->limit(1)->firstOrFail();

      return  $Platform->devices()->updateOrCreate(
         ['device_id' => $request->device_id],
         [
            'device_id' => $request->device_id,
            'timezome' => $request->timezome,
            'locale' => $request->locale,
            'country' => $request->country,
            'push_token' => (string) optional($request)->push_token,
         ],
      )->load('platform');
   }
}
