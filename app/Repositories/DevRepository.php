<?php

namespace App\Repositories;


use App\Models\Code;
use App\Models\Content\Article;
use App\Models\Content\Category;
use App\Models\Platform;
use App\Models\User;
use App\Repositories\Interfaces\DevRepositoryInterface;
use Auth;
use TelegramLog;

class DevRepository implements DevRepositoryInterface
{
   public function deleteUserByPhone($phone)
   {
      User::wherePhone($phone)->forceDelete();
      Code::wherePhone($phone)->delete();
      TelegramLog::sendMessage('Запрос на удалиление пользователя: ' . $phone);

      return true;
   }

   public function getTokenByPhone($phone)
   {

      $user = User::wherePhone($phone)->first();
      return optional($user->createToken('client'))->accessToken;
   }


   //SELF EXAMPLE
   public function storeArticle($request)
   {
      Auth::setUser(User::first());

      $article =  Article::create($request->all());

      if ($preview = $request->file('preview')) {
         $article->addMedia($preview)->toMediaCollection('preview');
      }

      $article->category()->associate(Category::first());

      $article->publication_at = now();

      $article->save();

      return $article;
   }


   public function listPlatform()
   {
      return Platform::orderBy('name')->paginate();
   }
}
