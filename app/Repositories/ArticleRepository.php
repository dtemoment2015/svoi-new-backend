<?php

namespace App\Repositories;


use App\Models\Code;
use App\Models\Content\Article;
use App\Models\User;
use App\Repositories\Interfaces\ArticleRepositoryInterface;
use TelegramLog;

class ArticleRepository implements ArticleRepositoryInterface
{

   public function listArticleForClient($request)
   {
      // делаю SELECT чтобы в списке BODY не выходил.
      return Article::select(
         'id',
         'rubric_id',
         'position',
         'name',
         'slug',
         'publication_at',
         'annotation'
      )
         ->whereActive(true)
         ->with('tags')
         ->with('categories')
         ->withCount('comments')
         ->orderBy('position')
         ->orderByDesc('publication_at')
         ->orderByDesc('id')
         ->paginate();
   }

   public function showArticleForClient($slug)
   {
      return Article::whereActive(true)
         ->whereSlug($slug)
         ->with('tags')
         ->with('categories')
         ->withCount('comments')
         ->limit(1)
         ->first();
   }

   public function listArticleCommentForClient($slug)
   {
      return Article::whereActive(true)
         ->whereSlug($slug)
         ->limit(1)
         ->firstOrFail()
         ->comments()
         ->with('user')
         ->orderByDesc('id')
         ->paginate();
   }

   public function storeArticleCommentForClient($request, $slug)
   {
      return Article::whereActive(true)
         ->whereSlug($slug)
         ->limit(1)
         ->firstOrFail()
         ->comments()
         ->create($request->all())
         ->load('user');
   }
}
