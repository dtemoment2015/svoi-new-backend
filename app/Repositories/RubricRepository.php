<?php

namespace App\Repositories;

use App\Models\Content\Rubric;
use App\Repositories\Interfaces\RubricRepositoryInterface;


class RubricRepository implements RubricRepositoryInterface
{

   public function listRubricForClient($request)
   {
      return Rubric::whereActive(true)
         ->paginate();
   }

   public function listRubricArticleForClient($slug, $request)
   {
      return Rubric::whereSlug($slug)
         ->limit(1)
         ->firstOrFail()
         ->articles()
         ->select(
            'id',
            'rubric_id',
            'position',
            'name',
            'slug',
            'publication_at',
            'annotation'
         )
         ->whereActive(true)
         ->with('tags')
         ->with('categories')
         ->withCount('comments')

         ->orderBy('position')
         ->orderByDesc('publication_at')
         ->orderByDesc('id')
         ->paginate();
   }
}
