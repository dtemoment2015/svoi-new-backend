<?php

namespace App\Repositories\Interfaces;

interface UserAuthRepositoryInterface
{
   public function logIn($request);
   public function registerVerification($request);
   public function registerConfirm($request);
   public function getUserByPhone($phone);
}
