<?php

namespace App\Repositories\Interfaces;

interface CategoryRepositoryInterface
{
   public function listCategoryForClient($request);

   public function showCategoryForClient($slug);
}
