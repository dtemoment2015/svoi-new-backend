<?php

namespace App\Repositories\Interfaces;

interface DevRepositoryInterface
{
   public function deleteUserByPhone($phone);
   public function getTokenByPhone($phone);
   public function storeArticle($request);
   public function listPlatform();
}
