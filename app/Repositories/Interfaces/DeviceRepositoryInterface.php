<?php

namespace App\Repositories\Interfaces;

interface DeviceRepositoryInterface
{
   public function storeDevice($request);
}
