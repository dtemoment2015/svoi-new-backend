<?php

namespace App\Repositories\Interfaces;

interface ArticleRepositoryInterface
{
   public function listArticleForClient($request);

   public function showArticleForClient($slug);

   public function listArticleCommentForClient($slug);

   public function storeArticleCommentForClient($request, $slug);
}
