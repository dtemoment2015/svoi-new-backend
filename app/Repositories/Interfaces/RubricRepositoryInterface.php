<?php

namespace App\Repositories\Interfaces;

interface RubricRepositoryInterface
{
   public function listRubricForClient($request);

   public function listRubricArticleForClient($slug, $request);
}
