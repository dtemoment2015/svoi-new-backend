<?php

namespace App\Repositories;


use App\Models\Code;
use App\Models\Content\Category;
use App\Models\User;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use TelegramLog;

class CategoryRepository implements CategoryRepositoryInterface
{

   public function listCategoryForClient($request)
   {
      return Category::whereActive(true)
         ->with('tags')
         ->with('categories')
         ->whereDoesntHave('category')
         ->paginate();
   }

   public function showCategoryForClient($slug)
   {
      return Category::whereActive(true)
         ->whereSlug($slug)
         ->with('tags')
         ->limit(1)
         ->first();
   }
}
