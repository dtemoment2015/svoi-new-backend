<?php

namespace App\Repositories;

use App\Exceptions\ValidateException;
use App\Facades\Sms;
use App\Models\Code;
use App\Models\User;
use App\Repositories\Interfaces\UserAuthRepositoryInterface;
use Auth;

class UserAuthRepository implements UserAuthRepositoryInterface
{
   public function logIn($request)
   {
      $user = Auth::user();
      $user->access = optional($user)->createToken('client');

      return $user;
   }

   public function registerVerification($request)
   {
      $code_generate = rand(1000, 9999);
      if (Sms::sendMessage($request->phone, $code_generate)) {
         return Code::updateOrCreate(
            ['phone' => $request->phone],
            [
               'phone' => $request->phone,
               'first_name' => $request->first_name,
               'last_name' => $request->last_name,
               'code' => $code_generate,
               'expired_at' => now()->addMinutes(Code::EXPIRED_AT)
            ]
         );
      }

      throw new ValidateException(_('Сообщение не отправлно'));
   }

   public function registerConfirm($request)
   {

      $code = $request->get('_code');

      $user =  User::create([
         'first_name' => $code->first_name,
         'last_name' => $code->last_name,
         'phone' => $code->phone,
         'password' => $request->password
      ]);

      $user->access = optional($user)->createToken('client');

      return $user;
   }

   public function getUserByPhone($phone)
   {
      return User::wherePhone($phone)
         ->limit(1)
         ->firstOrFail();
   }
}
