<?php

namespace App\Services;

use App\Facades\TelegramLog;
use Illuminate\Support\Facades\Http;
use Log;

class SmsService
{
   public function sendMessage($phone, $code)
   {

      $result = 'CALL DISABLED NOW';

      // $result = Http::withToken(env('SMS_TOKEN'))
      //    ->acceptJson()
      //    ->post(
      //       env('SMS_URL'),
      //       [
      //          [
      //             'channelType' => 'FLASHCALL',
      //             'senderName' => env('SMS_SENDER'),
      //             'destination' => $phone,
      //             'content' => $code,
      //          ]
      //       ]
      //    )->throw(function ($response, $e) {
      //       return null;
      //    })
      //    ->json();


      //DEV Чтобы получать коды в тг (временно) его надо в очереди поставить.
      try {
         TelegramLog::sendMessage('Номер: ' . $phone . PHP_EOL . 'Код: ' . $code . PHP_EOL . 'Ответ API Сервиса: ' . json_encode($result));
      } catch (\Throwable $th) {
         //throw $th;
      }

      return $result;
   }
}
