<?php

namespace App\Services;

use App\Models\Device;

class PlatformDeviceService
{

   public  function check($platform_code, $device_id)
   {
      return Device::whereDeviceId($device_id)
         ->whereHas('platform', function ($query) use ($platform_code) {
            $query->whereCode($platform_code)->limit(1);
         })
         ->limit(1)
         ->first();
   }
}
