<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class TelegramLogService
{
   public  function sendMessage(
      $text = 'Привет - тест от Дениса',
      $chat_id = -875046407
   ) {
      $txt = str_split($text, 3000);
      foreach ($txt as $t) {
         Http::acceptJson()
            ->post('https://api.telegram.org/bot' . env('TELEGRAM_LOG_TOKEN') . '/sendMessage', [
               'chat_id' =>  $chat_id,
               'text' =>  mb_convert_encoding($t, 'UTF-8', 'UTF-8'),
            ])->throw(function ($response, $e) {
               return null;
            })
            ->json();
      }
   }
}
