<?php

namespace App\Exceptions;

use Exception;
use stdClass;

class ValidateException extends Exception
{
    public $message = '';
    public $errors = [];

    public function __construct($message, $errors = new stdClass)
    {
        parent::__construct();

        $this->message = $message;
        $this->errors = $errors;
    }

    public function render($request)
    {
        return response()->json([
            "errors" => $this->errors,
            "message" => $this->message
        ], 422);
    }
}
