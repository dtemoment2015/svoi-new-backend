<?php

namespace App\Http\Requests\v1\Client;

use App\Rules\v1\PhoneRegexRule;
use App\Rules\v1\ValidateCodeRule;
use Illuminate\Foundation\Http\FormRequest;

class RegisterConfirmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
   
        return [
            'phone' => ['required', new PhoneRegexRule, 'unique:users,phone'],
            'code' => ['required', 'string', 'max:4', 'min:4', new ValidateCodeRule($this)],
            'password' => ['required', 'string', 'min:8', 'max:32']
        ];
    }
}
