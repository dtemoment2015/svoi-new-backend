<?php

namespace App\Http\Requests\v1\Client;

use App\Rules\v1\Client\CheckPhonePasswordRule;
use App\Rules\v1\PhoneRegexRule;
use Illuminate\Foundation\Http\FormRequest;

/** 
 *  @OA\Schema(schema="ClientLoginRequest")
 */


class LogInRequest extends FormRequest
{


    /**
     *  @OA\Property(
     *    property="phone",
     *    type="string",
     *    example="998903467899",
     *    description=""
     *  )
     *  
     *  @var string;
     */
    public $phone;

    /**
     *  @OA\Property(
     *    property="password",
     *    type="string",
     *    example="123213",
     *    description=""
     *  )
     *  @var string;
     */
    public $password;


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'phone' => ['required', new PhoneRegexRule],
            'password' => [
                'required',
                new CheckPhonePasswordRule($this->input('phone')),
            ],
        ];
    }
}
