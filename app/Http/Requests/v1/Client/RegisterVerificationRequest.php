<?php

namespace App\Http\Requests\v1\Client;

use App\Rules\v1\CheckCodeRule;
use App\Rules\v1\PhoneRegexRule;
use Illuminate\Foundation\Http\FormRequest;

class RegisterVerificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {

        return [
            'phone' => ['required', new PhoneRegexRule, new CheckCodeRule, 'unique:users,phone'],
            'first_name' => ['required', 'string', 'max:191', 'min:1'],
            'last_name' => ['required', 'string', 'max:191', 'min:1'],
        ];
    }
}
