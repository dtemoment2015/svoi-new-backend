<?php

namespace App\Http\Requests\v1;

use Illuminate\Foundation\Http\FormRequest;

class StoreDeviceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'platform_code' => ['required', 'string', 'exists:platforms,code'],
            'device_id' => ['required', 'string'],
            'locale' => ['required'],
            'timezone' => ['required'],
            'country' => ['required'],
        ];
    }
}
