<?php

namespace App\Http\Middleware\v1;

use App\Facades\PlatformDevice;
use Closure;
use Illuminate\Http\Request;

class DeviceMiddelware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($device = PlatformDevice::check(
            $request->header('platform_code'),
            $request->header('device_id')
        )) {
            $request->request->add(['_device' => $device]);
        } else {
            abort(403, 'Контент не досутпен');
        }

        return $next($request);
    }
}
