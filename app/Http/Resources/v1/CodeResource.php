<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 *  App\Http\Resources\v1\CodeResource
 *  @OA\Schema(schema="CodeResource")
 */

class CodeResource extends JsonResource
{
    /**
     *  
     *  @OA\Property(
     *    property="phone",
     *    type="string",
     *    example="998903467899",
     *    description="Номер телефона пользователя"
     *  ) 
     *  @var string
     *  
     *  @OA\Property(
     *    property="first_name",
     *    type="string",
     *    example="",
     *    description="Имя пользователя"
     *  ) 
     *  @var string
     *  
     *  @OA\Property(
     *    property="last_name",
     *    type="string",
     *    example="",
     *    description="Фамилия пользователя"
     *  ) 
     *  @var string
     *  
     *  @OA\Property(
     *    property="expired_at",
     *    type="datetime",
     *    example="2022-11-08T10:16:09.000000Z",
     *    description="Дата окончания токена"
     *  ) 
     *  @var string
     */

    public function toArray($request)
    {
        return [
            'phone' => $this->phone,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'expired_at' => $this->expired_at
        ];
    }
}
