<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 *  App\Http\Resources\v1\Client\UserResource
 * @OA\Schema(schema="ClientUserResource")
 */

class UserResource extends JsonResource
{

    /**
     * 
     *  @OA\Property(
     *    property="id",
     *    type="int",
     *    example="0",
     *    description="ID Пользователя"
     *  ) 
     *  
     *  @OA\Property(
     *    property="phone",
     *    type="string",
     *    example="998903467899",
     *    description="Номер телефона пользователя"
     *  ) 
     *  
     *  @OA\Property(
     *    property="email",
     *    type="string",
     *    example="test@test.ru",
     *    description="Email пользователя"
     *  ) 
     *  
     *  @OA\Property(
     *    property="first_name",
     *    type="string",
     *    example="",
     *    description="Имя пользователя"
     *  ) 
     *  
     *  @OA\Property(
     *    property="last_name",
     *    type="string",
     *    example="",
     *    description="Фамилия пользователя"
     *  ) 
     *  
     *  @OA\Property(
     *    property="active",
     *    type="boolean",
     *    example="false",
     *    description="Активность пользователя"
     *  ) 
     *  
     *  @OA\Property(
     *    property="middle_name",
     *    type="string",
     *    example="",
     *    description="Отчество пользователя"
     *  ) 
     */


    public function toArray($request)
    {
        return [
            'id' => (int) $this->id,
            'phone' => (string) $this->phone,
            'email' => (string) $this->email,
            'active' => (bool) $this->active,
            'first_name' => (string) $this->first_name,
            'last_name' => (string) $this->last_name,
            'middle_name' => (string) $this->middle_name,
        ];
    }

    public function withResponse($request, $response)
    {
        //ТОКЕН И ВРЕМЯ ЖИЗНИ ТОКЕНА В ЗАГОЛОВКЕ ОТПРАВЛЯЕТСЯ
        if (optional($this)->access) {
            $response->header('X-Access-Token', (string) optional($this->access)->accessToken);
            $response->header('X-Access-Token-Expires-At', (string) optional(optional($this->access)->token)['expires_at']);
        }
    }
}
