<?php

namespace App\Http\Resources\v1\Client;

use App\Http\Resources\v1\System\StatusResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 *  App\Http\Resources\v1\Client\ArticleResource
 *  @OA\Schema(schema="ClientArticleResource")
 */

class ArticleResource extends JsonResource
{

    /**
     * 
     *  @OA\Property(
     *    property="id",
     *    type="int",
     *    example="0",
     *    description="ID"
     *  ) 
     *  
     *  @OA\Property(
     *    property="name",
     *    type="string",
     *    example="Hello world",
     *    description=""
     *  ) 
     *  
     *  @OA\Property(
     *    property="slug",
     *    type="string",
     *    example="hello-world",
     *    description="Ссылка обращения до элемента"
     *  ) 
     *  
     *  @OA\Property(
     *    property="preview",
     *    type="string",
     *    example="https://image.jpg",
     *    description="Постер поста"
     *  ) 
     *  
     *  @OA\Property(
     *    property="annotation",
     *    type="string",
     *    example="Cupiditate expedita velit voluptates non voluptatum sit nostrum",
     *    description=""
     *  ) 
     *  
     *  @OA\Property(
     *    property="body",
     *    type="string",
     *    example="<p>Cupiditate expedita velit voluptates <br/> non voluptatum sit nostrum</p>",
     *    description="Не отображается в списке"
     *  ) 
     *  
     *  
     *  @OA\Property(
     *    property="comments_count",
     *    type="int",
     *    example="2",
     *    description="Количество комметов"
     *  ) 
     *  
     *   @OA\Property(
     *    property="publication_at",
     *    type="datetime",
     *    example="2022-11-08T10:16:09.000000Z",
     *    description="Дата публикации"
     *  ) 
     *  
     *  @OA\Property(
     *    property="rubric",
     *    type="object",
     *    ref="#/components/schemas/ClientRubricResource"
     *  ) 
     *  
     *  @OA\Property(
     *    property="tags",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/ClientTagResource"),
     *    description="Теги"
     *  ) 
     *  
     *  @OA\Property(
     *    property="categories",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/ClientCategoryResource"),
     *    description="Связанные катгории"
     *  ) 
     *  
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'name' => $this->name,
            'preview' => (string) $this->preview,
            'annotation' => $this->annotation,
            'body' => $this->when(isset($this->body), $this->body),
            'publication_at' => $this->publication_at,
            'status' =>  new StatusResource($this->whenLoaded('status')),
            'rubric' => new RubricResource($this->whenLoaded('rubric')),
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'tags' =>  TagResource::collection($this->whenLoaded('tags')),
            'position' => $this->position,
            'comments_count' => $this->comments_count,
        ];
    }
}
