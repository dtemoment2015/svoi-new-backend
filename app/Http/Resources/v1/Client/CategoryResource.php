<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 *  App\Http\Resources\v1\Client\CategoryResource
 *  @OA\Schema(schema="ClientCategoryResource")
 */

class CategoryResource extends JsonResource
{
    /**
     * 
     *  @OA\Property(
     *    property="id",
     *    type="int",
     *    example="0",
     *    description="ID"
     *  ) 
     *  
     *  @OA\Property(
     *    property="active",
     *    type="boolean",
     *    example="true",
     *    description="Активность новости"
     *  ) 
     *  
     *  @OA\Property(
     *    property="name",
     *    type="string",
     *    example="Категория 1",
     *    description="Название"
     *  ) 
     *  
     *  @OA\Property(
     *    property="position",
     *    type="int",
     *    example="0",
     *    description="Позиция"
     *  ) 
     *  
     *  @OA\Property(
     *    property="slug",
     *    type="string",
     *    example="kategoriya-1",
     *    description="Ссылка"
     *  ) 
     *  
     *  @OA\Property(
     *    property="tags",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/_ModelTag"),
     *    description="Теги"
     *  ) 
     *  
     *  @OA\Property(
     *    property="category",
     *    type="object",
     *    ref="#/components/schemas/_ModelCategory",
     *    description="Родительная категория категории"
     *  ) 
     *  
     *  @OA\Property(
     *    property="categories",
     *    type="array",
     *    @OA\Items(ref="#/components/schemas/_ModelCategory"),
     *    description="Дочерние категории"
     *  ) 
     * 
     */


    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'name' => $this->name,
            'category' =>  new CategoryResource($this->whenLoaded('category')),
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'tags' =>  TagResource::collection($this->whenLoaded('tags')),
            'position' => $this->position,
        ];
    }
}
