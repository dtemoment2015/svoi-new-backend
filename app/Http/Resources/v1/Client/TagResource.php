<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 *  App\Http\Resources\v1\Client\TagResource
 *  @OA\Schema(schema="ClientTagResource")
 */

class TagResource extends JsonResource
{
 
      /**
     * 
     *  @OA\Property(
     *    property="id",
     *    type="int",
     *    example="0",
     *    description="ID"
     *  ) 
     *  
     *  @OA\Property(
     *    property="name",
     *    type="string",
     *    example="Новинки",
     *    description="Название"
     *  ) 
     *  
     *  @OA\Property(
     *    property="slug",
     *    type="string",
     *    example="novinki",
     *    description="Название"
     *  ) 
     *  
     */
    
    public function toArray($request)
    {
        return  [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
        ];
    }
}
