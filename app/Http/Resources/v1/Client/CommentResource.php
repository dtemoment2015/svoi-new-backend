<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 *  App\Http\Resources\v1\Client\CommentResource
 *  @OA\Schema(schema="ClientCommentResource")
 */

class CommentResource extends JsonResource
{
      /**
     * 
     *  @OA\Property(
     *    property="id",
     *    type="int",
     *    example="0",
     *    description="ID"
     *  ) 
     *  
     *  @OA\Property(
     *    property="body",
     *    type="string",
     *    example="Супер! Мне очень нравится",
     *    description="Комментарий"
     *  ) 
     *  
     *  @OA\Property(
     *    property="user",
     *    type="object",
     *    ref="#/components/schemas/ClientUserResource",
     *    description="Кто оставил комментарий"
     *  ) 
     *  
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => (string) $this->name,
            'body' =>  $this->body,
            'comment' => new self($this->whenLoaded('comment')),
            'comments' => self::collection($this->whenLoaded('comments')),
            'user' => new UserResource($this->whenLoaded('user')),
            'created_at' => $this->created_at
        ];
    }
}
