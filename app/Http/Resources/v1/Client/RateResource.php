<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 *  App\Http\Resources\v1\Client\RateResource
 *  @OA\Schema(schema="ClientRateResource")
 */

class RateResource extends JsonResource
{
  
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'rate' => (int) $this->rate,
            'user' => new UserResource($this->whenLoaded('user'))
        ];
    }
}
