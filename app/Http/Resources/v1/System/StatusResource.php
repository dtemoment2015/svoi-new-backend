<?php

namespace App\Http\Resources\v1\System;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 *  App\Http\Resources\v1\System\StatusResource
 *  @OA\Schema(schema="SystemStatusResource")
 */

class StatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name
        ];
    }
}
