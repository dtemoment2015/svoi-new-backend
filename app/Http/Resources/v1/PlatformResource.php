<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 *  App\Http\Resources\v1\PlatformResource
 *  @OA\Schema(schema="PlatformResource")
 */

class PlatformResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            // 'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name
        ];
    }
}
