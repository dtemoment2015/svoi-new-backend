<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;


/**
 *  App\Http\Resources\v1\DeviceResource
 *  @OA\Schema(schema="DeviceResource")
 */

class DeviceResource extends JsonResource
{
 
    public function toArray($request)
    {
        return [
            'device_id' => $this->device_id,
            'locale' => $this->locale,
            'country' => $this->country,
            'push_token' => $this->push_token,
            'platform' => new PlatformResource($this->whenLoaded('platform'))
        ];
    }
}
