<?php

namespace App\Http\Controllers\v1\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Client\LogInRequest;
use App\Http\Requests\v1\Client\RegisterConfirmRequest;
use App\Http\Requests\v1\Client\RegisterVerificationRequest;
use App\Http\Resources\v1\Client\UserResource;
use App\Http\Resources\v1\CodeResource;
use App\Repositories\Interfaces\UserAuthRepositoryInterface;

class AuthController extends Controller
{

    public UserAuthRepositoryInterface $UserAuthRepository;

    public function __construct(UserAuthRepositoryInterface $UserAuthRepository)
    {
        $this->UserAuthRepository = $UserAuthRepository;
    }

    /**
     *  @OA\Post(
     *   security={{"platform_code": ""}, {"device_id": ""}},
     *   tags={"Client.Auth"},
     *   path="/v1/client/auth/login",
     *   summary="Вход в систему",
     *   @OA\RequestBody(
     *      required=true,
     *      description="Вход в систему",
     *      @OA\JsonContent(
     *            @OA\Schema(ref="#/components/schemas/ClientLoginRequest"),
     *            example={
     *                 "phone":"998903467899",
     *                 "password":"123123"
     *             }
     *          ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="Токен расположен в HEADER: X-Access-Token",
     *     @OA\JsonContent(
     *        @OA\Property(property="data", type="object", ref="#/components/schemas/ClientUserResource"),
     *     )
     *  ),
     *   @OA\Response(response=422, description="Неверный логин и пароль"),
     *   @OA\Response(response=403, description="Не установлено устройство"),
     * )
     */
    public function logIn(LogInRequest $request)
    {
        return new UserResource(
            $this->UserAuthRepository->logIn($request)
        );
    }

    /**
     *  @OA\Post(
     *   security={{"platform_code": ""}, {"device_id": ""}},
     *   tags={"Client.Auth"},
     *   path="/v1/client/auth/register/verification",
     *   summary="Регистрация (этап 1)",
     *   @OA\RequestBody(
     *      required=true,
     *      description="Запрос на звонок с кодом подтверждения",
     *      @OA\JsonContent(
     *            @OA\Schema(ref="#/components/schemas/ClientLoginRequest"),
     *            example={
     *                 "phone":"998903467899",
     *                 "first_name":"Denis",
     *                 "last_name":"Tsay",
     *             }
     *          ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="Код успешно отправлен на номер телефона",
     *     @OA\JsonContent(
     *        @OA\Property(property="data", type="object", ref="#/components/schemas/CodeResource"),
     *     )
     *  ),
     *   @OA\Response(response=422, description="Валидация формы"),
     *   @OA\Response(response=403, description="Не установлено устройство"),
     * )
     */
    public function registerVerification(RegisterVerificationRequest $request)
    {
        return new CodeResource(
            $this->UserAuthRepository->registerVerification($request)
        );
    }

    /**
     *  @OA\Post(
     *   security={{"platform_code": ""}, {"device_id": ""}},
     *   tags={"Client.Auth"},
     *   path="/v1/client/auth/register/confirm",
     *   summary="Регистрация (этап 2)",
     *   @OA\RequestBody(
     *      required=true,
     *      description="Регистрация (этап 1)",
     *      @OA\JsonContent(
     *            @OA\Schema(ref="#/components/schemas/ClientLoginRequest"),
     *            example={
     *                 "phone":"998903467899",
     *                 "code":"1234",
     *                 "password":"12345678"
     *             }
     *          ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="Токен расположен в HEADER: X-Access-Token",
     *     @OA\JsonContent(
     *        @OA\Property(property="data", type="object", ref="#/components/schemas/ClientUserResource"),
     *     )
     *  ),
     *   @OA\Response(response=422, description="Валидация формы"),
     *   @OA\Response(response=403, description="Не установлено устройство"),

     * )
     */
    public function registerСonfirm(RegisterConfirmRequest $request)
    {
        return new UserResource(
            $this->UserAuthRepository->registerConfirm($request)
        );
    }
}
