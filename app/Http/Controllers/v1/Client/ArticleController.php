<?php

namespace App\Http\Controllers\v1\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\Client\StoreCommentRequest;
use App\Http\Resources\v1\Client\ArticleResource;
use App\Http\Resources\v1\Client\CommentResource;
use App\Repositories\Interfaces\ArticleRepositoryInterface;
use Illuminate\Http\Request;

class ArticleController extends Controller
{

    public ArticleRepositoryInterface $ArticleRepository;

    public function __construct(ArticleRepositoryInterface $ArticleRepository)
    {
        $this->ArticleRepository = $ArticleRepository;
    }

    /**
     *  @OA\Get(
     *   security={{"platform_code": ""}, {"device_id": ""}},
     *   tags={"Client.Article"},
     *   path="/v1/client/articles",
     *   summary="Список всех статей",
     *   @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *         @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/ClientArticleResource")),
     *         @OA\Property(
     *             property="meta", 
     *             type="object",   
     *               @OA\Property(property="per_page", type="int", example="15"),
     *               @OA\Property(property="total", type="int", example="15"),
     *               @OA\Property(property="last_page", type="int", example="15"),
     *               @OA\Property(property="current_page", type="int", example="15")
     *         ),
     *     )
     *  ),
     *  @OA\Response(response=403, description="Не установлено устройство"),
     * )
     */
    public function index(Request $request)
    {
        return ArticleResource::collection(
            $this->ArticleRepository->listArticleForClient($request)
        );
    }

    /**
     *  @OA\Get(
     *   security={{"platform_code": ""}, {"device_id": ""}},
     *   tags={"Client.Article"},
     *   path="/v1/client/articles/{slug}",
     *       @OA\Parameter(
     *     name="slug",
     *     description="ссылка",
     *     required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *          ),
     *     ),
     *   summary="Карточка статьи",
     *   @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *         @OA\Property(property="data", type="object",  ref="#/components/schemas/ClientArticleResource"),
     *     )
     *  ),
     * )
     */
    public function show($slug)
    {
        return new ArticleResource(
            $this->ArticleRepository->showArticleForClient($slug)
        );
    }

    /**
     *  @OA\Get(
     *   security={{"platform_code": ""}, {"device_id": ""}},
     *   tags={"Client.Article"},
     *   path="/v1/client/articles/{slug}/comments",
     *   @OA\Parameter(
     *     name="slug",
     *     description="ссылка",
     *     required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *          ),
     *     ),
     *   summary="Список всех комментарий новости",
     *   @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *         @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/ClientCommentResource")),
     *         @OA\Property(
     *             property="meta", 
     *             type="object",   
     *               @OA\Property(property="per_page", type="int", example="15"),
     *               @OA\Property(property="total", type="int", example="15"),
     *               @OA\Property(property="last_page", type="int", example="15"),
     *               @OA\Property(property="current_page", type="int", example="15")
     *         ),
     *     )
     *  ),
     * )
     */
    public function indexComment($slug)
    {
        return CommentResource::collection(
            $this->ArticleRepository->listArticleCommentForClient($slug)
        );
    }

    /**
     *  @OA\Post(
     *   security={{"platform_code": ""}, {"device_id": ""}, {"Authorization" : ""}},
     *   tags={"Client.Article"},
     *   path="/v1/client/articles/{slug}/comments",
     *   summary="Установить комментарий",
     *   @OA\RequestBody(
     *      required=true,
     *      description="Запрос на звонок с кодом подтверждения",
     *      @OA\JsonContent(
     *            example={
     *                 "body":"Супер! Все получилось",
     *             }
     *          ),
     *     ),
     *   @OA\Response(
     *     response=201,
     *     description="Комментарий успешно добавлен",
     *     @OA\JsonContent(
     *        @OA\Property(property="data", type="object", ref="#/components/schemas/ClientCommentResource"),
     *     )
     *  ),
     *   @OA\Response(response=422, description="Валидация формы"),
     *   @OA\Response(response=401, description="Не авторизован"),
     * 
     * )
     */
    public function storeComment(StoreCommentRequest $request, $slug)
    {
        return new CommentResource(
            $this->ArticleRepository->storeArticleCommentForClient($request, $slug)
        );
    }
}
