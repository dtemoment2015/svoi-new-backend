<?php

namespace App\Http\Controllers\v1\Client;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Client\CategoryResource;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public CategoryRepositoryInterface $CategoryRepository;

    public function __construct(CategoryRepositoryInterface $CategoryRepository)
    {
        $this->CategoryRepository = $CategoryRepository;
    }

    /**
     *  @OA\Get(
     *   security={{"platform_code": ""}, {"device_id": ""}},
     *   tags={"Client.Category"},
     *   path="/v1/client/categories",
     *   summary="Список всех категорий",
     *   @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *         @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/ClientCategoryResource")),
     *         @OA\Property(
     *             property="meta", 
     *             type="object",   
     *               @OA\Property(property="per_page", type="int", example="15"),
     *               @OA\Property(property="total", type="int", example="15"),
     *               @OA\Property(property="last_page", type="int", example="15"),
     *               @OA\Property(property="current_page", type="int", example="15")
     *         ),
     *     )
     *  ),
     * )
     */
    public function index(Request $request)
    {
        return CategoryResource::collection(
            $this->CategoryRepository->listCategoryForClient($request)
        );
    }
}
