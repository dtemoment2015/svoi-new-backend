<?php

namespace App\Http\Controllers\v1\Client;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Client\ArticleResource;
use App\Http\Resources\v1\Client\RubricResource;
use App\Repositories\Interfaces\RubricRepositoryInterface;
use Illuminate\Http\Request;

class RubricController extends Controller
{

    public RubricRepositoryInterface $RubricRepository;

    public function __construct(RubricRepositoryInterface $RubricRepository)
    {
        $this->RubricRepository = $RubricRepository;
    }

    /**
     *  @OA\Get(
     *   security={{"platform_code": ""}, {"device_id": ""}},
     *   tags={"Client.Rubric"},
     *   path="/v1/client/rubrics",
     *   summary="Список всех рубрик",
     *   @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *         @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/ClientRubricResource")),
     *         @OA\Property(
     *             property="meta", 
     *             type="object",   
     *               @OA\Property(property="per_page", type="int", example="15"),
     *               @OA\Property(property="total", type="int", example="15"),
     *               @OA\Property(property="last_page", type="int", example="15"),
     *               @OA\Property(property="current_page", type="int", example="15")
     *         ),
     *     )
     *  ),
     * )
     */
    public function index(Request $request)
    {
        return RubricResource::collection(
            $this->RubricRepository->listRubricForClient($request)
        );
    }

    /**
     *  @OA\Get(
     *   security={{"platform_code": ""}, {"device_id": ""}},
     *   tags={"Client.Rubric"},
     *   path="/v1/client/rubrics/{slug}/articles",
     *   @OA\Parameter(
     *     name="slug",
     *     description="ссылка",
     *     required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string",
     *          ),
     *     ),
     *   summary="Список всех постов внутри рубрики",
     *   @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *         @OA\Property(property="data", type="array",  @OA\Items(ref="#/components/schemas/ClientArticleResource")),
     *         @OA\Property(
     *             property="meta", 
     *             type="object",   
     *               @OA\Property(property="per_page", type="int", example="15"),
     *               @OA\Property(property="total", type="int", example="15"),
     *               @OA\Property(property="last_page", type="int", example="15"),
     *               @OA\Property(property="current_page", type="int", example="15")
     *         ),
     *     )
     *  ),
     * )
     */
    public function indexArticle(Request $request, $slug)
    {
        return ArticleResource::collection(
            $this->RubricRepository->listRubricArticleForClient($slug, $request)
        );
    }
}
