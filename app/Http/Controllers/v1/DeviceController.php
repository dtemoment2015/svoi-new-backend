<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\StoreDeviceRequest;
use App\Http\Resources\v1\DeviceResource;
use App\Repositories\Interfaces\DeviceRepositoryInterface;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    public DeviceRepositoryInterface $DeviceRepository;

    public function __construct(DeviceRepositoryInterface $DeviceRepository)
    {
        $this->DeviceRepository = $DeviceRepository;
    }

    /**
     *  @OA\Post(
     *   tags={"Device"},
     *   path="/v1/devices",
     *   summary="Регистрация устройства",
     *   @OA\RequestBody(
     *      required=true,
     *      description="platform_code - список можно посмотреть в DEV.platforms.",
     *      @OA\JsonContent(
     *            example={
     *                "device_id": "5FE23DE34LVL",
     *                "platform_code":"ios",
     *                "locale": "ru",
     *                "country": "ru",
     *                "timezone": 18000,
     *                "push_token" : "klemwnfjwbeirokpqdlfjnhbsdujisko;dljnhbvfiudojpsklj"
     *             }
     *          ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="Далее необходимо добавить в HEADER на все запросы device_id и platform_code",
     *     @OA\JsonContent(
     *        @OA\Property(property="data", type="object", ref="#/components/schemas/_ModelDevice"),
     *     )
     *  ),

     *   @OA\Response(response=422, description="запонены не до конца данные"),
     * )
     */
    public function store(StoreDeviceRequest $request)
    {
        return new DeviceResource(
            $this->DeviceRepository->storeDevice($request)
        );
    }
}
