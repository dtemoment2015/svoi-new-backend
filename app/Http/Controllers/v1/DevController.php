<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\PlatformResource;
use App\Repositories\Interfaces\DevRepositoryInterface;
use Illuminate\Http\Request;

class DevController extends Controller
{
    public DevRepositoryInterface $devRepository;

    public function __construct(DevRepositoryInterface $devRepository)
    {
        $this->devRepository = $devRepository;
    }

    /**
     *  @OA\Post(
     *   tags={"Dev"},
     *   path="/v1/dev/users/delete",
     *   summary="Удаление пользователя с системы",
     *   @OA\RequestBody(
     *      required=true,
     *      description="",
     *      @OA\JsonContent(
     *            example={
     *                 "phone":"998903467899",
     *             }
     *          ),
     *     ),
     *   @OA\Response(
     *        response=200,
     *        description="Пользователь удален",
     *   ),
     * )
     */
    public function deleteUserData(Request $request)
    {
        return response([
            'status' => (bool) $this->devRepository->deleteUserByPhone($request->phone)
        ]);
    }

    /**
     *  @OA\Post(
     *   tags={"Dev"},
     *   path="/v1/dev/users/token",
     *   summary="Получить Bearer token по номеру телефона",
     *   @OA\RequestBody(
     *      required=true,
     *      description="",
     *      @OA\JsonContent(
     *            example={
     *                 "phone":"998903467899",
     *             }
     *          ),
     *     ),
     *   @OA\Response(
     *        response=200,
     *        description="Токен в строке",
     *   ),
     * )
     */

    public function getUserToken(Request $request)
    {
        return response([
            'token' => $this->devRepository->getTokenByPhone($request->phone)
        ]);
    }


    /**
     *  @OA\GET(
     *   tags={"Dev"},
     *   path="/v1/dev/platforms",
     *   summary="Получить список платформ",
     *   @OA\Response(
     *        response=200,
     *        description="актуальный список платформ",
     *   ),
     * )
     */
    public function listPlatform()
    {
        return PlatformResource::collection(
            $this->devRepository->listPlatform()
        );
    }
}
