<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


/**
 * @OA\Info(
 *         version="1.0",
 *         title="SVOI API",
 *         description="Документация SVOI
 * Во всех ответах приходит объект: `data`, кроме ошибок: В ошибках `401` имеется только поле `message`; в ошибке `422` имеются поля: `errors и message`.
 * Для авторизованных запросов необходимо вставить в HEADER `Authorization ` Bearer токен полученный при авторизации. Пример:
 *     Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbM3MjlkIn0.eyJhdWQiOiI2IiwianRpIjoiYThlYjY3NGVjMzIyMmU3MWY5Nzk3MzMwMjU
 * Токен можно получить при входе в систему или регистрации. Сам токен будет расположен в ответе HEADER: 
 *      X-Access-Token: eyJ0eXAiOiJKV1QiLCJhbM3MjlkIn0.eyJhdWQiOiI2IiwianRpIjoiYThlYjY3NGVjMzIyMmU3MWY5Nzk3MzMwMjU
 *      X-Access-Token-Expires-At: 2023-11-08 10:13:18
 *  Важно! На всех запросах, кроме DEV и DEVICE - необходимо отправлять в HEADER platform_code и device_id. Иначе будет приходить  `403 ошибка`  
 *      platform_code: ios
 *      device_id: DFIE3232dD
 * На всех запросах в HEADER ОБЯЗАТЕЛЬНО установить:
 *     Accept: application/json"
 * ,
 *     
 *     
 *         @OA\Contact(
 *             email="support@svoi.club"
 *         )
 *     )
 *
 * @OA\SecurityScheme(
 *         securityScheme="Authorization",
 *         type="apiKey",
 *         description="Пример: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjI0OT",
 *         name="Authorization",
 *         in="header",
 *     )
 * @OA\SecurityScheme(
 *         securityScheme="platform_code",
 *         type="apiKey",
 *         description="Код платформы",
 *         name="platform_code",
 *         in="header",
 *     )
 * @OA\SecurityScheme(
 *         securityScheme="device_id",
 *         type="apiKey",
 *         description="ID устройства",
 *         name="device_id",
 *         in="header",
 *     )
 *     
 *     
 *     
 * @OA\Server(
 *     url="https://new.vsk-trust.store/api",
 *     description="API DEVELOPER SERVER"
 * )
 * @OA\Server(
 *     url="http://localhost:8000/api",
 *     description="API LOCAL SERVER"
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
