<?php

namespace Database\Seeders;

use App\Models\Action\Comment;
use App\Models\Content\Article;
use App\Models\Content\Category;
use App\Models\Content\Tag;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::factory()
            ->count(10)
            ->has(
                Category::factory()->count(rand(4, 6))->has(
                    Article::factory()->count(rand(10, 15))
                        ->has(
                            Comment::factory()->count(rand(3, 7))
                        )
                        ->has(
                            Tag::factory()->count(rand(3, 7))
                        )
                )
            )
            ->create();
    }
}
