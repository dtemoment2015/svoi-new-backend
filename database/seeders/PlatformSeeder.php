<?php

namespace Database\Seeders;

use App\Models\Platform;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PlatformSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public $startData = [
        [
            'code' => 'ios',
            'name' => 'iOS устройства',
        ],
        [
            'code' => 'android',
            'name' => 'Android устройства',
        ],
        [
            'code' => 'web',
            'name' => 'Браузер',
        ],
        [
            'code' => 'admin',
            'name' => 'Административная панель',
        ],
    ];

    public function run()
    {
        foreach ($this->startData as $data) {
            Platform::firstOrCreate(
                ['code' => $data['code']],
                [
                    'code' => $data['code'],
                    'name' => $data['name'],
                ]
            );
        }
    }
}
