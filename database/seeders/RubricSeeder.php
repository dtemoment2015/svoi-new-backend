<?php

namespace Database\Seeders;

use App\Models\Content\Rubric;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RubricSeeder extends Seeder
{
    public $startData = [
        [
            'slug' => 'news',
            'name' => 'Новости',
        ],
        [
            'slug' => 'events',
            'name' => 'События',
        ],
        [
            'slug' => 'contests',
            'name' => 'Конкурсы',
        ],
    ];

    public function run()
    {
        foreach ($this->startData as $data) {
            Rubric::firstOrCreate(
                ['slug' => $data['slug']],
                [
                    'slug' => $data['slug'],
                    'name' => $data['name'],
                    'active' => 1,
                ]
            );
        }
    }
}
