<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->timestamp('publication_at');
            $table->boolean('active')->default(0);
            $table->boolean('moderated')->default(0);
            $table->foreignId('status_id')->nullable()->constrained('statuses');
            $table->foreignId('rubric_id')->nullable()->constrained('rubrics');
            $table->string('name');
            $table->string('slug')->nullable()->unique()->index();
            $table->text('annotation')->nullable();
            $table->longText('body')->nullable();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->bigInteger('position')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
};
