<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->id();
            $table->string('device_id');
            $table->foreignId('platform_id')->constrained('platforms');
            $table->unique(['device_id', 'platform_id']);
            $table->string('locale', 2)->default('ru');
            $table->string('country', 4)->default('ru');
            $table->integer('timezone')->default(0);
            $table->text('push_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
};
