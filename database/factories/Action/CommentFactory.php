<?php

namespace Database\Factories\Action;

use App\Models\Action\Comment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Action\Comment>
 */
class CommentFactory extends Factory
{

    protected $model = Comment::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'body' => fake()->sentence(10),
            'user_id' => User::get()->random()->id,
            'active' => 1,
            'moderated' => 1,
        ];
    }
}
