<?php

namespace Database\Factories\Content;

use App\Models\Content\Rubric;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Rubric>
 */
class RubricFactory extends Factory
{

    protected $model = Rubric::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'active' => 1,
            'name' => fake()->sentence(3),
            'slug' => fake()->unique()->slug(2),
        ];
    }
}
