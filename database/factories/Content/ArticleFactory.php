<?php

namespace Database\Factories\Content;

use App\Models\Content\Article;
use App\Models\Content\Rubric;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\=Article>
 */
class ArticleFactory extends Factory
{

    protected $model = Article::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'active' => 1,
            'moderated' => 1,
            'publication_at' => now(),
            'name' => fake()->sentence(3),
            'annotation' => fake()->sentence(10),
            'body' => '<div class="fake-html"><h2>' . fake()->sentence(3) . '</h2>' . '<p>' . fake()->sentence(400) . '</p>' . '<p>' . fake()->sentence(400) . '</p></div>',
            'user_id' => User::get()->random()->id,
            'rubric_id' => Rubric::get()->random()->id,
            //'preview_url' => 'https://pixabay.com/get/gaa8e9c805c366eb0737796b95c4240e7be376b8c41b8845f02ebb58a6d481f4a5d2e87d32cb4325ff852a7913b2d0dda5b744667108ce689cec35c9f175a134fd056d131bc0dde3aca351b7e21d3a6eb_1280.jpg',
        ];
    }
}
