<?php

namespace Database\Factories\Content;

use App\Models\Content\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class TagFactory extends Factory
{

    protected $model = Tag::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'active' => 1,
            'name' => fake()->sentence(2),
            'slug' => fake()->unique()->slug(1),
            'user_id' => User::get()->random()->id,
        ];
    }
}
