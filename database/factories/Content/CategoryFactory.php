<?php

namespace Database\Factories\Content;

use App\Models\Content\Category;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class CategoryFactory extends Factory
{

    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'active' => 1,
            'name' => fake()->sentence(3),
            'user_id' => User::get()->random()->id
        ];
    }
}
