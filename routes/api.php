<?php

use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => 'v1'
    ],
    function () {
        //DEVICE
        Route::group(
            [
                'prefix' => 'devices',
            ],
            function () {
                Route::controller(App\Http\Controllers\v1\DeviceController::class)->group(function () {
                    Route::post('', 'store');
                });
            }
        );
        //CLIENT
        Route::group(
            [
                'prefix' => 'client',
                'middleware' => 'device'
            ],
            function () {
                //AUTH
                Route::group(
                    [
                        'prefix' => 'auth',
                    ],
                    function () {
                        Route::controller(App\Http\Controllers\v1\Client\AuthController::class)->group(function () {
                            Route::post('login', 'logIn');
                            Route::post('register/verification', 'registerVerification');
                            Route::post('register/confirm', 'registerСonfirm');
                        });
                    }
                );
                Route::group(
                    [
                        'prefix' => 'categories',
                    ],
                    function () {
                        Route::controller(App\Http\Controllers\v1\Client\CategoryController::class)->group(function () {
                            Route::get('/', 'index');
                        });
                    }
                );
                Route::group(
                    [
                        'prefix' => 'articles',
                    ],
                    function () {
                        Route::controller(App\Http\Controllers\v1\Client\ArticleController::class)->group(function () {
                            Route::get(null, 'index');
                            Route::get('{slug}', 'show');

                            //comments
                            Route::get('{slug}/comments', 'indexComment');
                            Route::post('{slug}/comments', 'storeComment')->middleware('auth:user');
                        });
                    }
                );
                Route::group(
                    [
                        'prefix' => 'rubrics',
                    ],
                    function () {
                        Route::controller(App\Http\Controllers\v1\Client\RubricController::class)->group(function () {
                            Route::get(null, 'index');
                            Route::get('{slug}/articles', 'indexArticle');
                        });
                    }
                );
            },
        );


        //DEVELOP
        Route::group(
            [
                'prefix' => 'dev',
            ],
            function () {
                Route::controller(App\Http\Controllers\v1\DevController::class)->group(function () {
                    Route::post('users/delete', 'deleteUserData');
                    Route::post('users/token', 'getUserToken');
                    Route::get('platforms', 'listPlatform');
                });
            }
        );
    },
);
